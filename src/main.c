#include <vector.h>

int main() {
	vector3D v1, v2;
	
	printf("Ingrese coordenada x de v1 :");
        scanf("%f",&v1.x);
        printf("Ingrese coordenada y de v1 :");
        scanf("%f",&v1.y);
        printf("Ingrese coordenada z de v1 :");
        scanf("%f",&v1.z);

	printf("Ahora, Ingrese coordenada x de v2 :");
	scanf("%f",&v2.x);
        printf("Ingrese coordenada y de v2 :"); 
        scanf("%f",&v2.y);
        printf("Ingrese coordenada z de v2 :");
        scanf("%f",&v2.z);

        printf("Los vectores ingresados son : ");
        printf("V1={%.1f ,%.1f ,%.1f} , ",v1.x,v1.y,v1.z);
        printf("V2={%.1f ,%.1f ,%.1f} \n",v2.x,v2.y,v2.z);
	
	printf("El producto escalar entre v1 y v2 es %.1f \n",dotproduct(v1,v2));

	vector3D v=crossproduct(v1,v2);	
	printf("El producto vectorial es V={%.1f ,%.1f ,%.1f} \n",v.x,v.y,v.z);

	printf("Sus magnitudes son |V1|=%.1f y |V2|=%.1f \n",magnitud(v1),magnitud(v2));	
	printf("Finalmente V1 y V2 %s ortogonales \n", esOrtogonal(v1,v2) ? "son" : "no son");
}
