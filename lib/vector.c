/* implementar aquí las funciones requeridas */
#include <vector.h>
#include <math.h>

float dotproduct(vector3D v1, vector3D v2){
	return v1.x*v2.x+v1.y*v2.y+v1.z*v2.z;
}

vector3D crossproduct(vector3D v1, vector3D v2){
	vector3D v;
	v.x=(v1.y*v2.z)-(v2.y*v1.z);
	v.y=(v2.x*v1.z)-(v1.x*v2.z);
	v.z=(v1.x*v2.y)-(v2.x*v1.y);
	return v;
}

float magnitud(vector3D v){
	return sqrt(dotproduct(v,v));
}

int esOrtogonal(vector3D v1, vector3D v2){
	if(dotproduct(v1,v2)==0){
		return 1;
	}
	return 0;
}
