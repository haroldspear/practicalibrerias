# Este es el archivo Makefile de la práctica, editarlo como sea neceario

TODO=vector.o ./lib/libvector.a ./lib/libvector.so main.o
CC=gcc
IDIR=./include
CFLAGS=-I$(IDIR)
SRC=./src/main.c
LIB=./lib/vector.c
DEPS=$(IDIR)/vector.h

LIBS=-lm

# Target que se ejecutará por defecto con el comando make
all: $(TODO) 

vector.o : $(LIB) $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

./lib/libvector.a : vector.o
	ar rcs $@ $^

./lib/libvector.so : $(LIB) $(DEPS)
	gcc -shared -fPIC -o $@ $^ $(CFLAGS)
	 
main.o : $(SRC) ./lib/libvector.a $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

# Target que compilará el proyecto usando librerías estáticas
static:
	gcc -static -o ejecutableS main.o ./lib/libvector.a $(LIBS)
	
# Target que compilará el proyecto usando librerías dinámicas
dynamic:
	gcc -o ejecutableD $(SRC) ./lib/libvector.so $(CFLAGS) $(LIBS)

# Limpiar archivos temporales
clean:
	$(RM) *.o ejecutableS ejecutableD
